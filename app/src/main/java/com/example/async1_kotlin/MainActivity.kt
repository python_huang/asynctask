package com.example.async1_kotlin

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView

/*AsyncTask 必須在 UI 主執行緒載入(JELLY_BEAN 版本開始會自動執行此事)。
必須在 UI 主執行緒建立 AsyncTask。
必須在 UI 主執行緒呼叫 AsyncTask.execute()。
不要自行呼叫 onPreExecute()，onPostExecute()，doInBackground()，onProgressUpdate()。
AsyncTask 只能執行一次。*/

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun go1(v: View) {
        Job1Task().execute()
    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Job1Task : AsyncTask<Void?, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            try {
                Thread.sleep(5000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            val info = findViewById<TextView>(R.id.info)
            info.text = "Go1完成"
        }
    }

    fun go2(v: View) {
        Job2Task().execute(3)
    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Job2Task : AsyncTask<Int, Void, Void>() {

        override fun doInBackground(vararg params: Int?): Void? {
            try {
                Thread.sleep((params[0]!! * 1000).toLong())
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            val info = findViewById<TextView>(R.id.info)
            info.text = "Go2完成"
        }
    }

    fun go3(v: View) {
        Job3Task().execute(6)
    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Job3Task : AsyncTask<Int, Int, Void>() {
        //onPreExecute--AsyncTask執行前的準備工作
        override fun doInBackground(vararg params: Int?): Void? {
            //要執行的程式碼
            for (i in params[0]!! downTo 1) {
                publishProgress(i)
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
            return null
        }

        override fun onProgressUpdate(vararg values: Int?) {
            //用來顯示目前進度
            super.onProgressUpdate(*values)
            val info = findViewById<TextView>(R.id.info)
            info.text = values[0].toString()
        }

        override fun onPostExecute(aVoid: Void?) {
            //執行完的結果
            super.onPostExecute(aVoid)
            val info = findViewById<TextView>(R.id.info)
            info.text = "Go3完成"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

}